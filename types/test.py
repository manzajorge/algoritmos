x = 'global'


print(x)
def f():
    x = 'enclosing'

    def g():
        print(locals())
        print(x)
    print(locals())
    g()

f()
print(x)
