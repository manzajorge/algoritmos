# Exercise 1
"""
Writes the necessary code to return the first and the last item from any known sequence.
The expected output has to be the same type of the sequence!
If I have the string "abc" it has to return "ac".
If the input is a list [1, 2, 3, 4] it has to return [1, 4]
Same for tuple ("hello", "whatever", "world") it has to return ("hello", "world")
Note: Remember you can retrieve any index from a sequence with [] notation.
"""

# exercise 2
"""
Write a code block that loops over a sequence only for the last N elements!
Example: I have a iterable "This is a new string" and 
I only want to iterate over the last 5 positions the output should be: "tring"
"""

# exercise 3
"""
Write a piece of code that given a list or a tuple of numbers return a tuple of two elements
The first element in the tuple is the sum of odd numbers
The second element in the tuple is the sum of even numbers
[10, 20, 30, 40, 50, 60] --> (90, 120)
"""

# exercise 4
"""
Code a function game. Guessing the number. We are going to write a game(function) to guess the number between 0 and 100
We will write a function that accepts no arguments and randomly generates a number between 0 and 100 
    (random.randint(0, 100))
We will then let the user to input a number and we will respond back three different choices:
 - Too high
 - Too Low
 - Just right! 
"""

# exercise 5
"""
Create a function to join numbers from an iterable of numbers. Use list comprehension
[1,2,3,4,5,6,7,8,9,19] --> "1,2,3,4,5,6,7,8,9,10"
"""

# exercise 6
"""
Given a string sum all the elements that are numbers.
"Hello 10 world20 90" -> 100. world20 is not a valid digit. 
We can use list comprehension here too.
"""

# exercise 7
"""
Write a function that adds the string 'ub' before every vowel.
"wind" -> wubind ---> w{ub}ind 
"This is a text" --> "Thubis ubis uba tubext"
"""

# Exercise 8
"""
We need to find the longest word of an input text
"This is an example" -> example
"""


# Exercise 9
"""
We want to hide sensible data from a given string. The sensible data is going to be a list of names that we should hide
and change them for '*'. We need to set as much '*' as the length of the value.

text="this is a text" hide=["text"] --> "this is a ****" 
"""


# Exercise 10
"""
Given a list of dictionaries describing objects from a factory
We need to order them. each dict will have ALWAYS the attributes: name/weight/group
Return the same list of dictionaries ordered by the name of the tool and then from the weight
If passed an empty list, then return an empty list
"""

# exercise 11
"""
Given an array of integers, find the numbers that appears an odd number of times.
There will always be only one integer that appears an odd number of times.

Examples
[7] should return 7, because it occurs 1 time (which is odd).
[0] should return 0, because it occurs 1 time (which is odd).
[1,2] should return [1,2], because it occurs 1 time (which is odd).
[0,1,0,1,0] should return 0, because it occurs 3 times (which is odd).
[1,2,2,3,3,3,4,3,3,3,2,2,1] should return 4, because it appears 1 time (which is odd).

"""

# Exercise 12
"""
In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.
"""

