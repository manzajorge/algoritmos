
def calcular_resultado(
        ganados: int,
        perdidos: int,
        empatados: int,
        puntos_ganar: int = 3,
        puntos_perder: int = 1,
        puntos_empatar: int = 0) -> int:
    """
    Método que calcula los puntos que tiene un equipo de futbol dados

    :param ganados: Número de partidos que ha ganado el equipo
    :param perdidos: Número de partidos que ha perdido
    :param empatados: Número de partidos empatados
    :param puntos_ganar: Puntos por ganar un partido. Por defecto son 3.
    :param puntos_perder: Puntos por perder un partido. Por defecto es 1.
    :param puntos_empatar: Puntos por empatar un partido. Por defecto es 0.
    :return: Total de puntos conseguidos
    """
    ppganados = puntos_ganar * ganados
    pperdidos = puntos_perder * perdidos
    pempatados = puntos_empatar * empatados
    return pperdidos + ppganados + pempatados


print(calcular_resultado(1, 1, 1))
print(calcular_resultado(3, 2, 1, puntos_perder=-2))
print(calcular_resultado(1, 0, 1, 5, 3, 1))



