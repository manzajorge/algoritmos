import uuid


class StockPiece:

    """
    Defines a stock piece of a map
    """
    def __init__(self, x: int, y: int, length: float, width: float):
        """
        Constructor for class Stock Piec
        :param x:       x coordinate, in mm
        :param y:       y coordiante, in mm
        :param length:  length of stock piece, in m
        :param width:   width of stock piece, in m
        """
        self.id = uuid.uuid4()  # unique identifier
        self.x = x
        self.y = y
        self.length = length
        self.width = width

    def area(self) -> float:
        """
        Calculates area of the stock piece
        :return: area in m2
        """
        return self.width * self.length


sp1 = StockPiece(0, 0, 1, 2)
