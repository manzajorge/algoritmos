from typing import Dict, Callable


def mult_by_two(num: int) -> int:
    return num * 2


def add_one(num: int) -> int:
    return num + 1


def update_inventory(inventory: Dict[str, int], func: Callable) -> Dict[str, int]:
    """
    Applies a function to the quantity of the inventory
    :param inventory:   Inventory dictionary
    :param func:        Function to apply to quantity
    :return:            Updated inventory
    """
    for item, quantity in inventory.items():
        inventory[item] = func(quantity)
    return inventory


if __name__ == '__main__':
    input_inventory = {
        "ham": 2,
        "cheese": 3,
        "milk": 5
    }

    print(update_inventory(input_inventory, add_one))
    print(update_inventory(input_inventory, mult_by_two))
